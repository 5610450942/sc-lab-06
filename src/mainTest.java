
public class mainTest {
	 static int x;
	public static void main(String[] args){
	System.out.println("Test 1. \n>> new Super Class :");
	Super sup = new Super();
	System.out.println(">> new Sub Class :");
	Sub   sub = new Sub();
	System.out.println(">> new overload Suber Class :");
	Sub subA = new Sub("Overload");
	
	
	
	System.out.println("Test 2. \n >> abstract class");
	abSubA ab1 =new abSubA();
	abSubB ab2 =new abSubB();
	abSuper ab3 =new abSubA();
	abSuper ab4 =new abSubB();
	mainTest mTest = new mainTest();
	mTest.overLoading(ab1);
	mTest.overLoading(ab2);
	mTest.overLoading(ab3);
	mTest.overLoading(ab4);


	System.out.println("Test 3. \n>> Local var shadow :");
	 x = 5;
     System.out.println("x = " + x);
     int x;
     x = 10;
     System.out.println("x = " + x);
     System.out.println("mainTest.x = " +mainTest.x);
     System.out.println("access modifier method  class pakage");
     testVar var1 =new testVar();
     System.out.println(var1.a);
     System.out.println(var1.varMethod());
	}
	
	
	public void overLoading(abSubA a){
		System.out.println("passing sub A ");
	}
	public void overLoading(abSubB b){
		System.out.println("passing Sub B ");
	}
	public void overLoading(abSuper s){
		System.out.println("Polymor ");
	}
	
}
